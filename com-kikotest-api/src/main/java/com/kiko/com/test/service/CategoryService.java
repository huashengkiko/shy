package com.kiko.com.test.service;

import com.deepexi.util.pageHelper.PageBean;
import com.kiko.com.test.domain.eo.Category;
import java.util.*;

public interface CategoryService {

    PageBean<Category> findPage(Category eo, Integer page, Integer size);

    List<Category> findAll(Category eo);

    Category detail(String pk);

    Boolean update(String pk, Category eo);

    Boolean create(Category eo);

    Boolean delete(String... pk);
}