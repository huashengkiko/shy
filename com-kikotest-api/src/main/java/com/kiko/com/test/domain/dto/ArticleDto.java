package com.kiko.com.test.domain.dto;

import com.kiko.com.test.domain.eo.Article;
import java.util.Collection;
import java.util.List;
import java.util.Date;
import java.io.Serializable;

/**
 * @desc article
 * @author admin
 */
public class ArticleDto implements Serializable {
    private String id;

    private String categoryId;

    private String wechatPublistNo;

    private String tenantId;

    private Date createdAt;

    private Date createdBy;

    private Date updatedBy;

    private Date updatedAt;

    private Boolean dr;

    private String title;

    private String author;

    private String status;

    private String thumbMediaId;

    private String thumbMediaUrl;

    private String digest;

    private Boolean showCoverPic;

    private String contentSourceUrl;

    private Boolean needOpenComment;

    private Boolean onlyFansCanComment;

    private Date lastUpdatedTime;

    private Date releaseTime;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String extJson;


    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setCategoryId(String categoryId){
        this.categoryId = categoryId;
    }

    public String getCategoryId(){
        return this.categoryId;
    }

    public void setWechatPublistNo(String wechatPublistNo){
        this.wechatPublistNo = wechatPublistNo;
    }

    public String getWechatPublistNo(){
        return this.wechatPublistNo;
    }

    public void setTenantId(String tenantId){
        this.tenantId = tenantId;
    }

    public String getTenantId(){
        return this.tenantId;
    }

    public void setCreatedAt(Date createdAt){
        this.createdAt = createdAt;
    }

    public Date getCreatedAt(){
        return this.createdAt;
    }

    public void setCreatedBy(Date createdBy){
        this.createdBy = createdBy;
    }

    public Date getCreatedBy(){
        return this.createdBy;
    }

    public void setUpdatedBy(Date updatedBy){
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedBy(){
        return this.updatedBy;
    }

    public void setUpdatedAt(Date updatedAt){
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt(){
        return this.updatedAt;
    }

    public void setDr(Boolean dr){
        this.dr = dr;
    }

    public Boolean getDr(){
        return this.dr;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }

    public void setAuthor(String author){
        this.author = author;
    }

    public String getAuthor(){
        return this.author;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }

    public void setThumbMediaId(String thumbMediaId){
        this.thumbMediaId = thumbMediaId;
    }

    public String getThumbMediaId(){
        return this.thumbMediaId;
    }

    public void setThumbMediaUrl(String thumbMediaUrl){
        this.thumbMediaUrl = thumbMediaUrl;
    }

    public String getThumbMediaUrl(){
        return this.thumbMediaUrl;
    }

    public void setDigest(String digest){
        this.digest = digest;
    }

    public String getDigest(){
        return this.digest;
    }

    public void setShowCoverPic(Boolean showCoverPic){
        this.showCoverPic = showCoverPic;
    }

    public Boolean getShowCoverPic(){
        return this.showCoverPic;
    }

    public void setContentSourceUrl(String contentSourceUrl){
        this.contentSourceUrl = contentSourceUrl;
    }

    public String getContentSourceUrl(){
        return this.contentSourceUrl;
    }

    public void setNeedOpenComment(Boolean needOpenComment){
        this.needOpenComment = needOpenComment;
    }

    public Boolean getNeedOpenComment(){
        return this.needOpenComment;
    }

    public void setOnlyFansCanComment(Boolean onlyFansCanComment){
        this.onlyFansCanComment = onlyFansCanComment;
    }

    public Boolean getOnlyFansCanComment(){
        return this.onlyFansCanComment;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime){
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public Date getLastUpdatedTime(){
        return this.lastUpdatedTime;
    }

    public void setReleaseTime(Date releaseTime){
        this.releaseTime = releaseTime;
    }

    public Date getReleaseTime(){
        return this.releaseTime;
    }

    public void setExt1(String ext1){
        this.ext1 = ext1;
    }

    public String getExt1(){
        return this.ext1;
    }

    public void setExt2(String ext2){
        this.ext2 = ext2;
    }

    public String getExt2(){
        return this.ext2;
    }

    public void setExt3(String ext3){
        this.ext3 = ext3;
    }

    public String getExt3(){
        return this.ext3;
    }

    public void setExt4(String ext4){
        this.ext4 = ext4;
    }

    public String getExt4(){
        return this.ext4;
    }

    public void setExtJson(String extJson){
        this.extJson = extJson;
    }

    public String getExtJson(){
        return this.extJson;
    }
}

