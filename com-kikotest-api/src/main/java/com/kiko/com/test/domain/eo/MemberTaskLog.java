package com.kiko.com.test.domain.eo;


import com.deepexi.util.mapper.SuperEntity;

import java.util.*;
import java.io.Serializable;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;


/**
 * @desc member_task_log
 * @author admin
 */
//@ApiModel(description = "member_task_log")
public class MemberTaskLog implements Serializable{

    //@ApiModelProperty(value = "id")
    private String id;

    //@ApiModelProperty(value = "租户id")
    private String tenantId;

    //@ApiModelProperty(value = "会员id")
    private String memberId;

    //@ApiModelProperty(value = "任务id")
    private String taskId;

    //@ApiModelProperty(value = "次数")
    private Integer  times;

    //@ApiModelProperty(value = "周期")
    private Integer  cycle;

    //@ApiModelProperty(value = "创建时间")
    private Date createdAt;

    //@ApiModelProperty(value = "创建人")
    private String createdBy;

    //@ApiModelProperty(value = "更新时间")
    private Date updatedAt;

    //@ApiModelProperty(value = "更新人")
    private String updatedBy;

    //@ApiModelProperty(value = "逻辑删除")
    private Boolean dr;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setTenantId(String tenantId){
        this.tenantId = tenantId;
    }

    public String getTenantId(){
        return this.tenantId;
    }

    public void setMemberId(String memberId){
        this.memberId = memberId;
    }

    public String getMemberId(){
        return this.memberId;
    }

    public void setTaskId(String taskId){
        this.taskId = taskId;
    }

    public String getTaskId(){
        return this.taskId;
    }

    public void setTimes(Integer  times){
        this.times = times;
    }

    public Integer  getTimes(){
        return this.times;
    }

    public void setCycle(Integer  cycle){
        this.cycle = cycle;
    }

    public Integer  getCycle(){
        return this.cycle;
    }

    public void setCreatedAt(Date createdAt){
        this.createdAt = createdAt;
    }

    public Date getCreatedAt(){
        return this.createdAt;
    }

    public void setCreatedBy(String createdBy){
        this.createdBy = createdBy;
    }

    public String getCreatedBy(){
        return this.createdBy;
    }

    public void setUpdatedAt(Date updatedAt){
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt(){
        return this.updatedAt;
    }

    public void setUpdatedBy(String updatedBy){
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy(){
        return this.updatedBy;
    }

    public void setDr(Boolean dr){
        this.dr = dr;
    }

    public Boolean getDr(){
        return this.dr;
    }


}

