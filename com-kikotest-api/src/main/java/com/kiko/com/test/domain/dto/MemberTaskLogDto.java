package com.kiko.com.test.domain.dto;

import com.kiko.com.test.domain.eo.MemberTaskLog;
import java.util.Collection;
import java.util.List;
import java.util.Date;
import java.io.Serializable;

/**
 * @desc member_task_log
 * @author admin
 */
public class MemberTaskLogDto implements Serializable {
    private String id;

    private String tenantId;

    private String memberId;

    private String taskId;

    private Integer  times;

    private Integer  cycle;

    private Date createdAt;

    private String createdBy;

    private Date updatedAt;

    private String updatedBy;

    private Boolean dr;


    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setTenantId(String tenantId){
        this.tenantId = tenantId;
    }

    public String getTenantId(){
        return this.tenantId;
    }

    public void setMemberId(String memberId){
        this.memberId = memberId;
    }

    public String getMemberId(){
        return this.memberId;
    }

    public void setTaskId(String taskId){
        this.taskId = taskId;
    }

    public String getTaskId(){
        return this.taskId;
    }

    public void setTimes(Integer  times){
        this.times = times;
    }

    public Integer  getTimes(){
        return this.times;
    }

    public void setCycle(Integer  cycle){
        this.cycle = cycle;
    }

    public Integer  getCycle(){
        return this.cycle;
    }

    public void setCreatedAt(Date createdAt){
        this.createdAt = createdAt;
    }

    public Date getCreatedAt(){
        return this.createdAt;
    }

    public void setCreatedBy(String createdBy){
        this.createdBy = createdBy;
    }

    public String getCreatedBy(){
        return this.createdBy;
    }

    public void setUpdatedAt(Date updatedAt){
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt(){
        return this.updatedAt;
    }

    public void setUpdatedBy(String updatedBy){
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy(){
        return this.updatedBy;
    }

    public void setDr(Boolean dr){
        this.dr = dr;
    }

    public Boolean getDr(){
        return this.dr;
    }
}

