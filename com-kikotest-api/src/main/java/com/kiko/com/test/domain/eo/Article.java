package com.kiko.com.test.domain.eo;


import com.deepexi.util.mapper.SuperEntity;

import java.util.*;
import java.io.Serializable;
//import io.swagger.annotations.ApiModel;
//import io.swagger.annotations.ApiModelProperty;


/**
 * @desc article
 * @author admin
 */
//@ApiModel(description = "article")
public class Article implements Serializable{

    //@ApiModelProperty(value = "id")
    private String id;

    //@ApiModelProperty(value = "类目id")
    private String categoryId;

    //@ApiModelProperty(value = "公众号id")
    private String wechatPublistNo;

    //@ApiModelProperty(value = "租户id")
    private String tenantId;

    //@ApiModelProperty(value = "创建时间")
    private Date createdAt;

    //@ApiModelProperty(value = "创建人")
    private Date createdBy;

    //@ApiModelProperty(value = "更新人")
    private Date updatedBy;

    //@ApiModelProperty(value = "更新时间")
    private Date updatedAt;

    //@ApiModelProperty(value = "逻辑删除")
    private Boolean dr;

    //@ApiModelProperty(value = "标题")
    private String title;

    //@ApiModelProperty(value = "作者")
    private String author;

    //@ApiModelProperty(value = "状态")
    private String status;

    //@ApiModelProperty(value = "封面图片素材id")
    private String thumbMediaId;

    //@ApiModelProperty(value = "封面图片url")
    private String thumbMediaUrl;

    //@ApiModelProperty(value = "摘要")
    private String digest;

    //@ApiModelProperty(value = "是否显示封面")
    private Boolean showCoverPic;

    //@ApiModelProperty(value = "图文消息的原文地址，即点击“阅读原文”后的URL")
    private String contentSourceUrl;

    //@ApiModelProperty(value = "是否打开评论")
    private Boolean needOpenComment;

    //@ApiModelProperty(value = "是否粉丝才可评论")
    private Boolean onlyFansCanComment;

    //@ApiModelProperty(value = "最后更新时间")
    private Date lastUpdatedTime;

    //@ApiModelProperty(value = "发布时间")
    private Date releaseTime;

    //@ApiModelProperty(value = "预留字段")
    private String ext1;

    //@ApiModelProperty(value = "预留字段")
    private String ext2;

    //@ApiModelProperty(value = "预留字段")
    private String ext3;

    //@ApiModelProperty(value = "预留字段")
    private String ext4;

    //@ApiModelProperty(value = "预留字段")
    private String extJson;

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return this.id;
    }

    public void setCategoryId(String categoryId){
        this.categoryId = categoryId;
    }

    public String getCategoryId(){
        return this.categoryId;
    }

    public void setWechatPublistNo(String wechatPublistNo){
        this.wechatPublistNo = wechatPublistNo;
    }

    public String getWechatPublistNo(){
        return this.wechatPublistNo;
    }

    public void setTenantId(String tenantId){
        this.tenantId = tenantId;
    }

    public String getTenantId(){
        return this.tenantId;
    }

    public void setCreatedAt(Date createdAt){
        this.createdAt = createdAt;
    }

    public Date getCreatedAt(){
        return this.createdAt;
    }

    public void setCreatedBy(Date createdBy){
        this.createdBy = createdBy;
    }

    public Date getCreatedBy(){
        return this.createdBy;
    }

    public void setUpdatedBy(Date updatedBy){
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedBy(){
        return this.updatedBy;
    }

    public void setUpdatedAt(Date updatedAt){
        this.updatedAt = updatedAt;
    }

    public Date getUpdatedAt(){
        return this.updatedAt;
    }

    public void setDr(Boolean dr){
        this.dr = dr;
    }

    public Boolean getDr(){
        return this.dr;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle(){
        return this.title;
    }

    public void setAuthor(String author){
        this.author = author;
    }

    public String getAuthor(){
        return this.author;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return this.status;
    }

    public void setThumbMediaId(String thumbMediaId){
        this.thumbMediaId = thumbMediaId;
    }

    public String getThumbMediaId(){
        return this.thumbMediaId;
    }

    public void setThumbMediaUrl(String thumbMediaUrl){
        this.thumbMediaUrl = thumbMediaUrl;
    }

    public String getThumbMediaUrl(){
        return this.thumbMediaUrl;
    }

    public void setDigest(String digest){
        this.digest = digest;
    }

    public String getDigest(){
        return this.digest;
    }

    public void setShowCoverPic(Boolean showCoverPic){
        this.showCoverPic = showCoverPic;
    }

    public Boolean getShowCoverPic(){
        return this.showCoverPic;
    }

    public void setContentSourceUrl(String contentSourceUrl){
        this.contentSourceUrl = contentSourceUrl;
    }

    public String getContentSourceUrl(){
        return this.contentSourceUrl;
    }

    public void setNeedOpenComment(Boolean needOpenComment){
        this.needOpenComment = needOpenComment;
    }

    public Boolean getNeedOpenComment(){
        return this.needOpenComment;
    }

    public void setOnlyFansCanComment(Boolean onlyFansCanComment){
        this.onlyFansCanComment = onlyFansCanComment;
    }

    public Boolean getOnlyFansCanComment(){
        return this.onlyFansCanComment;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime){
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public Date getLastUpdatedTime(){
        return this.lastUpdatedTime;
    }

    public void setReleaseTime(Date releaseTime){
        this.releaseTime = releaseTime;
    }

    public Date getReleaseTime(){
        return this.releaseTime;
    }

    public void setExt1(String ext1){
        this.ext1 = ext1;
    }

    public String getExt1(){
        return this.ext1;
    }

    public void setExt2(String ext2){
        this.ext2 = ext2;
    }

    public String getExt2(){
        return this.ext2;
    }

    public void setExt3(String ext3){
        this.ext3 = ext3;
    }

    public String getExt3(){
        return this.ext3;
    }

    public void setExt4(String ext4){
        this.ext4 = ext4;
    }

    public String getExt4(){
        return this.ext4;
    }

    public void setExtJson(String extJson){
        this.extJson = extJson;
    }

    public String getExtJson(){
        return this.extJson;
    }


}

