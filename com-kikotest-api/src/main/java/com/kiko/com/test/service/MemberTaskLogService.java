package com.kiko.com.test.service;

import com.deepexi.util.pageHelper.PageBean;
import com.kiko.com.test.domain.eo.MemberTaskLog;
import java.util.*;

public interface MemberTaskLogService {

    PageBean<MemberTaskLog> findPage(MemberTaskLog eo, Integer page, Integer size);

    List<MemberTaskLog> findAll(MemberTaskLog eo);

    MemberTaskLog detail(String pk);

    Boolean update(String pk, MemberTaskLog eo);

    Boolean create(MemberTaskLog eo);

    Boolean delete(String... pk);
}