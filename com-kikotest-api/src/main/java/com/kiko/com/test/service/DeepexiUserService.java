package com.kiko.com.test.service;

import com.deepexi.util.pageHelper.PageBean;
import com.kiko.com.test.domain.eo.DeepexiUser;
import java.util.*;

public interface DeepexiUserService {

    PageBean<DeepexiUser> findPage(DeepexiUser eo, Integer page, Integer size);

    List<DeepexiUser> findAll(DeepexiUser eo);

    DeepexiUser detail(Integer  pk);

    Boolean update(Integer  pk, DeepexiUser eo);

    Boolean create(DeepexiUser eo);

    Boolean delete(Integer ... pk);
}