package com.kiko.com.test.service;

import com.deepexi.util.pageHelper.PageBean;
import com.kiko.com.test.domain.eo.Article;
import java.util.*;

public interface ArticleService {

    PageBean<Article> findPage(Article eo, Integer page, Integer size);

    List<Article> findAll(Article eo);

    Article detail(String pk);

    Boolean update(String pk, Article eo);

    Boolean create(Article eo);

    Boolean delete(String... pk);
}