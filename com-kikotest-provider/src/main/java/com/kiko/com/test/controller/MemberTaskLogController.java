package com.kiko.com.test.controller;

import com.kiko.com.test.service.MemberTaskLogService;
import com.kiko.com.test.domain.eo.MemberTaskLog;
import com.deepexi.util.config.Payload;
import com.deepexi.util.constant.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;

@Service
@Path("/api/v1/memberTaskLogs")
@Consumes({ContentType.APPLICATION_JSON_UTF_8, ContentType.TEXT_XML_UTF_8})
@Produces({ContentType.APPLICATION_JSON_UTF_8, ContentType.TEXT_XML_UTF_8})
public class MemberTaskLogController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MemberTaskLogService memberTaskLogService;

    @GET
    @Path("/")
    public Payload findPage(@BeanParam MemberTaskLog eo,
                            @QueryParam("page") @DefaultValue("1") Integer page,
                            @QueryParam("size") @DefaultValue("10") Integer size) {
        return new Payload(memberTaskLogService.findPage(eo, page, size));
    }

    @GET
    @Path("/list")
    public Payload findAll(@BeanParam MemberTaskLog eo) {
        return new Payload(memberTaskLogService.findAll(eo));
    }

    @GET
    @Path("/{id:[a-zA-Z0-9]+}")
    public Payload detail(@PathParam("id") String pk) {
        return new Payload(memberTaskLogService.detail(pk));
    }

    @POST
    @Path("/")
    public Payload create(MemberTaskLog eo) {
        return new Payload(memberTaskLogService.create(eo));
    }

    @PUT
    @Path("/{id:[a-zA-Z0-9]+}")
    public Payload update(@PathParam("id") String pk, MemberTaskLog eo) {
        return new Payload(memberTaskLogService.update(pk, eo));
    }

    @DELETE
    @Path("/{id:[a-zA-Z0-9]+}")
    public Payload delete(@PathParam("id") String pk) {
        return new Payload(memberTaskLogService.delete(pk));
    }

    @DELETE
    @Path("/")
    public Payload delete(String[] pks) {
        return new Payload(memberTaskLogService.delete(pks));
    }
}
