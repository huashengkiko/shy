package com.kiko.com.test.controller;

import com.kiko.com.test.service.DeepexiUserService;
import com.kiko.com.test.domain.eo.DeepexiUser;
import com.deepexi.util.config.Payload;
import com.deepexi.util.constant.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;

@Service
@Path("/api/v1/deepexiUsers")
@Consumes({ContentType.APPLICATION_JSON_UTF_8, ContentType.TEXT_XML_UTF_8})
@Produces({ContentType.APPLICATION_JSON_UTF_8, ContentType.TEXT_XML_UTF_8})
public class DeepexiUserController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DeepexiUserService deepexiUserService;

    @GET
    @Path("/")
    public Payload findPage(@BeanParam DeepexiUser eo,
                            @QueryParam("page") @DefaultValue("1") Integer page,
                            @QueryParam("size") @DefaultValue("10") Integer size) {
        return new Payload(deepexiUserService.findPage(eo, page, size));
    }

    @GET
    @Path("/list")
    public Payload findAll(@BeanParam DeepexiUser eo) {
        return new Payload(deepexiUserService.findAll(eo));
    }

    @GET
    @Path("/{id:[a-zA-Z0-9]+}")
    public Payload detail(@PathParam("id") Integer  pk) {
        return new Payload(deepexiUserService.detail(pk));
    }

    @POST
    @Path("/")
    public Payload create(DeepexiUser eo) {
        return new Payload(deepexiUserService.create(eo));
    }

    @PUT
    @Path("/{id:[a-zA-Z0-9]+}")
    public Payload update(@PathParam("id") Integer  pk, DeepexiUser eo) {
        return new Payload(deepexiUserService.update(pk, eo));
    }

    @DELETE
    @Path("/{id:[a-zA-Z0-9]+}")
    public Payload delete(@PathParam("id") Integer  pk) {
        return new Payload(deepexiUserService.delete(pk));
    }

    @DELETE
    @Path("/")
    public Payload delete(Integer [] pks) {
        return new Payload(deepexiUserService.delete(pks));
    }
}
