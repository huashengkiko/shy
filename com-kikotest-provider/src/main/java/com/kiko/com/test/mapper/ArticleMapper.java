package com.kiko.com.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kiko.com.test.domain.eo.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleMapper extends BaseMapper<Article> {

    List<Article> findList(@Param("eo") Article eo);

    int deleteByIds(String... pks);
}
