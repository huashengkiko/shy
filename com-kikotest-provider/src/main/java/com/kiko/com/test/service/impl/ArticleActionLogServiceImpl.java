package com.kiko.com.test.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kiko.com.test.service.ArticleActionLogService;
import com.kiko.com.test.domain.eo.ArticleActionLog;
import com.kiko.com.test.extension.AppRuntimeEnv;
import com.kiko.com.test.mapper.ArticleActionLogMapper;
import com.deepexi.util.pageHelper.PageBean;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
@Service(version = "${demo.service.version}")
public class ArticleActionLogServiceImpl implements ArticleActionLogService {

    private static final Logger logger = LoggerFactory.getLogger(ArticleActionLogServiceImpl.class);

    @Autowired
    private ArticleActionLogMapper articleActionLogMapper;

    @Autowired
    private AppRuntimeEnv appRuntimeEnv;

    @Override
    public PageBean findPage(ArticleActionLog eo, Integer page, Integer size) {
        PageHelper.startPage(page, size);
        List<ArticleActionLog> list = articleActionLogMapper.findList(eo);
        return new PageBean<>(list);
    }

    @Override
    public List<ArticleActionLog> findAll(ArticleActionLog eo) {
        List<ArticleActionLog> list = articleActionLogMapper.findList(eo);
        return list;
    }
    @Override
    public ArticleActionLog detail(String pk) {
        return articleActionLogMapper.selectById(pk);
    }

    @Override
    public Boolean create(ArticleActionLog eo) {
        int result = articleActionLogMapper.insert(eo);
        if (result > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean update(String pk,ArticleActionLog eo) {
        eo.setId(pk);
        int result = articleActionLogMapper.updateById(eo);
        if (result > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean delete(String...pk) {
        int result = articleActionLogMapper.deleteByIds(pk);
        if (result > 0) {
            return true;
        }
        return false;
    }

}