package com.kiko.com.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kiko.com.test.domain.eo.MemberTaskLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MemberTaskLogMapper extends BaseMapper<MemberTaskLog> {

    List<MemberTaskLog> findList(@Param("eo") MemberTaskLog eo);

    int deleteByIds(String... pks);
}
