package com.kiko.com.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kiko.com.test.domain.eo.DeepexiUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DeepexiUserMapper extends BaseMapper<DeepexiUser> {

    List<DeepexiUser> findList(@Param("eo") DeepexiUser eo);

    int deleteByIds(Integer ... pks);
}
