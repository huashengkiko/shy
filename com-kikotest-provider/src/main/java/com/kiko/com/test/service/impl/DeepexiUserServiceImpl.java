package com.kiko.com.test.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kiko.com.test.service.DeepexiUserService;
import com.kiko.com.test.domain.eo.DeepexiUser;
import com.kiko.com.test.extension.AppRuntimeEnv;
import com.kiko.com.test.mapper.DeepexiUserMapper;
import com.deepexi.util.pageHelper.PageBean;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
@Service(version = "${demo.service.version}")
public class DeepexiUserServiceImpl implements DeepexiUserService {

    private static final Logger logger = LoggerFactory.getLogger(DeepexiUserServiceImpl.class);

    @Autowired
    private DeepexiUserMapper deepexiUserMapper;

    @Autowired
    private AppRuntimeEnv appRuntimeEnv;

    @Override
    public PageBean findPage(DeepexiUser eo, Integer page, Integer size) {
        PageHelper.startPage(page, size);
        List<DeepexiUser> list = deepexiUserMapper.findList(eo);
        return new PageBean<>(list);
    }

    @Override
    public List<DeepexiUser> findAll(DeepexiUser eo) {
        List<DeepexiUser> list = deepexiUserMapper.findList(eo);
        return list;
    }
    @Override
    public DeepexiUser detail(Integer  pk) {
        return deepexiUserMapper.selectById(pk);
    }

    @Override
    public Boolean create(DeepexiUser eo) {
        int result = deepexiUserMapper.insert(eo);
        if (result > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean update(Integer  pk,DeepexiUser eo) {
        eo.setId(pk);
        int result = deepexiUserMapper.updateById(eo);
        if (result > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean delete(Integer ...pk) {
        int result = deepexiUserMapper.deleteByIds(pk);
        if (result > 0) {
            return true;
        }
        return false;
    }

}