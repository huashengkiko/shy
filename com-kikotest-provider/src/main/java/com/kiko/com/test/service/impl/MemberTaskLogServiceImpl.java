package com.kiko.com.test.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kiko.com.test.service.MemberTaskLogService;
import com.kiko.com.test.domain.eo.MemberTaskLog;
import com.kiko.com.test.extension.AppRuntimeEnv;
import com.kiko.com.test.mapper.MemberTaskLogMapper;
import com.deepexi.util.pageHelper.PageBean;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;


@Component
@Service(version = "${demo.service.version}")
public class MemberTaskLogServiceImpl implements MemberTaskLogService {

    private static final Logger logger = LoggerFactory.getLogger(MemberTaskLogServiceImpl.class);

    @Autowired
    private MemberTaskLogMapper memberTaskLogMapper;

    @Autowired
    private AppRuntimeEnv appRuntimeEnv;

    @Override
    public PageBean findPage(MemberTaskLog eo, Integer page, Integer size) {
        PageHelper.startPage(page, size);
        List<MemberTaskLog> list = memberTaskLogMapper.findList(eo);
        return new PageBean<>(list);
    }

    @Override
    public List<MemberTaskLog> findAll(MemberTaskLog eo) {
        List<MemberTaskLog> list = memberTaskLogMapper.findList(eo);
        return list;
    }
    @Override
    public MemberTaskLog detail(String pk) {
        return memberTaskLogMapper.selectById(pk);
    }

    @Override
    public Boolean create(MemberTaskLog eo) {
        int result = memberTaskLogMapper.insert(eo);
        if (result > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean update(String pk,MemberTaskLog eo) {
        eo.setId(pk);
        int result = memberTaskLogMapper.updateById(eo);
        if (result > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Boolean delete(String...pk) {
        int result = memberTaskLogMapper.deleteByIds(pk);
        if (result > 0) {
            return true;
        }
        return false;
    }

}